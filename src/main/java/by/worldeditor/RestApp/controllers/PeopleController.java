package by.worldeditor.RestApp.controllers;

import by.worldeditor.RestApp.dto.PersonDTO;
import by.worldeditor.RestApp.rmodels.Person;
import by.worldeditor.RestApp.service.PeopleService;
import by.worldeditor.RestApp.util.PersonErrorResponse;
import by.worldeditor.RestApp.util.PersonNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/people")
public class PeopleController {

    private final PeopleService peopleService;
    private final ModelMapper modelMapper;


    @Autowired
    public PeopleController(PeopleService peopleService, ModelMapper modelMapper) {
        this.peopleService = peopleService;
        this.modelMapper = modelMapper;
    }


    @PostMapping
    public ResponseEntity<HttpStatus> create (@RequestBody PersonDTO personDTO){
        peopleService.save(convertToPerson(personDTO));
        return ResponseEntity.ok(HttpStatus.OK);
    }


    @GetMapping
    public List<PersonDTO> getPeople(){
        return peopleService.getPeople().stream().map(this::converToPersonDTO)
                .collect(Collectors.toList());

    }

    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable("id") int id){
        Optional<Person> foundPerson = Optional.ofNullable(peopleService.getPersonById(id));
        return foundPerson.orElseThrow(PersonNotFoundException::new);
    }

    @PutMapping("/{id}")
    public Person updatePerson(@PathVariable("id") int id, Person person){
        return peopleService.updatePerson(id, person);
    }

    @ExceptionHandler
    private ResponseEntity<PersonErrorResponse> handleException(){
        PersonErrorResponse response = new PersonErrorResponse("Person with id wasn't found",
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }


    private Person convertToPerson(PersonDTO personDTO) {
        return modelMapper.map(personDTO, Person.class);
    }

    private PersonDTO converToPersonDTO(Person person){
        return modelMapper.map(person, PersonDTO.class);
    }

}
