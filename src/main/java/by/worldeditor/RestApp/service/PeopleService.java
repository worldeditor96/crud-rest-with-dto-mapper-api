package by.worldeditor.RestApp.service;

import by.worldeditor.RestApp.dto.PersonDTO;
import by.worldeditor.RestApp.repository.PeopleRepository;
import by.worldeditor.RestApp.rmodels.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class PeopleService {

    private final PeopleRepository peopleRepository;

    @Autowired
    public PeopleService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    public List<Person> getPeople(){
        List <Person> people = peopleRepository.findAll();
        return people;
    }

    public Person getPersonById(int id){
        return peopleRepository.findById(id).orElse(null);
    }

    public Person createPerson(Person createdPerson){
        Person newPerson = createdPerson;
        return newPerson;
    }

    public Person updatePerson(int id, Person updatingPerson){
        Person updatePerson = peopleRepository.findById(id).orElse(null);
        updatePerson.setAge(updatingPerson.getAge());
        updatePerson.setName(updatingPerson.getName());
        updatePerson.setEmail(updatePerson.getEmail());

        return updatePerson;
    }

    public void deletePerson(int id){
        peopleRepository.delete(peopleRepository.findById(id).orElse(null));
    }

    @Transactional
    public Person save(Person person) {
        enrichPerson(person);
        return peopleRepository.save(person);
    }

    private void enrichPerson(Person person) {
        person.setCreatedAt(LocalDateTime.now());
        person.setUpdatedAt(LocalDateTime.now());
        person.setCreatedWho("Admin is created this person");
    }
}
